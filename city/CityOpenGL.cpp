#include "glut.h"
#include <math.h>
#include <time.h>
#include <stdlib.h>     /* srand, rand */
#include <iostream>
#include "TextureHandler.h"
#include "ColorHandler.h"


#define PI 3.1415926535898
#define RADIUS 2

const int GSIZE = 200;	// 200
const int WIDTH = 600;	// 600
const int HEIGHT = 600; // 600

double gridBK[GSIZE][GSIZE]={0};
double grid[GSIZE][GSIZE]={0};
double gridElements[GSIZE][GSIZE]={0};
double roadDriveArray[GSIZE][9]={0};
bool Is1st3D = false;

double angle=PI,dangle=0.0,start=0;
double eyex=1,eyey=10,eyez=20;
double dx=0,dy=0,dz=0;
double sightx=0,sighty= -0.2,sightz = -1;
double speed = 0;
double myCarx=-1,myCary=8,myCarz=15;
double myCar_angle=-1,myCar_speed=0,myCar_ang_speed=0;
bool inCar = false;
int lastOption = 1;
double temp = 0;
int viewFrom = 0;
int Menu1_x, Menu2_x, Menu3_x, Menu1_y, Menu2_y, Menu3_y, Menu_size, Menu_Selected;

// function declartions

// opengl functions
void init(); 
void mouse(int button,int state,int x, int y);
void SpecialKey(int key,int x,int y);
void Keyboard(unsigned char key, int x, int y);
void idle();
void display2D();
void display3D();

// set terrain functions
void Smooth();
void UpdateTerrain1();
void UpdateTerrain2();
void UpdateTerrain3();

void SetColor(double h);
void SetColor2d(double h);

// element drawing functions
void DrawMenu();
void DrawLand();
void DrawLand2d();
void DrawWheel(float width);
void DrawCar();
void LandDown(int x, int y);
void LandUp(int x, int y);
void SetHouses(int x, int y);
void Draw3dCylBlock(int n, double topR, double bottomR,int tXrepeat, double YStart, double YEnd, int txnum);
void DrawHouse3d(int style);
void DrawElements();
void DrawRoad(int x1, int y1, int x2, int y2);

// logic and drawing functions
void DrawHouses();
void DrawRoads();
void Add2RoadDrive(double x, double y,double x1,double y1,double h1,double a1);
void MoveCar(double x, double y, bool IsLastPoint, double x1, double y1, double h1, double a1);
void resetCamera();
void menu(int option);
// end of function declartions


void init()
{
	int i;

	InitTextures();

	for(int i1=0; i1 < GSIZE; i1++){
		for(int j1=0; j1 < GSIZE; j1++){
		gridElements[i1][j1] = 0;
		}
	}
	Menu_size = 10;
	Menu1_x = -(GSIZE/2) + 2;
	Menu2_x = -(GSIZE/2) + 2;
	Menu3_x = -(GSIZE/2) + 2;
	Menu1_y = -(GSIZE/2) + 2;
	Menu2_y = Menu1_y + Menu_size + 2;
	Menu3_y = Menu2_y + Menu_size + 2;

	glClearColor(Background.RED,Background.GREEN,Background.BLUE,Background.alpha); // set background color
	glEnable(GL_DEPTH_TEST);

	srand((unsigned)time(NULL));

	for(i=1;i<200;i++)
		UpdateTerrain3();
	for(i=1;i<400;i++)
		UpdateTerrain1();
	for(i=1;i<50;i++)
		UpdateTerrain2();
	Smooth();
	for(i=1;i<5;i++)
		UpdateTerrain2();

}
double GetDistanceBetweenPoints(double X1, double Y1, double X2, double Y2)
{
	double a = X1 - X2;
	double b = Y1 - Y2;
	double distance = sqrt(a * a + b * b);
	return distance;
}
// low-pass filter
void Smooth()
{
	double tmp[GSIZE][GSIZE] = {0};
	int i,j;
	for(i=1;i<GSIZE-1;i++)
		for(j=1;j<GSIZE-1;j++)
		{
			tmp[i][j] = (grid[i-1][j-1] + grid[i-1][j] +grid[i-1][j+1] +
				grid[i][j-1] + 4*grid[i][j] +grid[i][j+1]+
				grid[i+1][j-1] + grid[i+1][j] +grid[i+1][j+1])/12;
		}

	for(i=1;i<GSIZE-1;i++)
		for(j=1;j<GSIZE-1;j++)
			grid[i][j] = tmp[i][j];
}
void UpdateTerrain1()
{
	double a,b,delta = 0.1;
	int x1,x2,y1,y2,x,y;

	x1 = rand()% GSIZE;
	y1 = rand()% GSIZE;
	x2 = rand()% GSIZE;
	y2 = rand()% GSIZE;

	if(rand()%2==0) delta = -delta;

	if(x1!=x2)
	{
		a = (y2-y1)/(double)(x2-x1) ;
		b = y1 - a*x1;
		for(y=0;y<GSIZE;y++)
			for(x=0;x<GSIZE;x++)
			{
				if(y>a*x+b) grid[y][x]+=delta;
				else grid[y][x] -= delta;
			}
	}
}
void UpdateTerrain2()
{
	int x1,y1,points = 1500,direction;
	double delta = 0.1;
	x1 = rand()% GSIZE;
	y1 = rand()% GSIZE;
	if(rand()%2==0) delta = -delta;

	while(points>0)
	{
		grid[y1][x1] += delta;
		direction = rand()%4;
		switch(direction)
		{
		case 0: // up
			y1--;
			break;
		case 1: // right
			x1++;
			break;
		case 2: // down
			y1++;
			break;
		case 3: // left
			x1--;
			break;
		} 
		x1 = (x1+GSIZE) % GSIZE;
		y1 = (y1+GSIZE) % GSIZE;
		points--;
	}
}
void UpdateTerrain3()
{
	int x,y,r;
	int i,j;
	double dist,alpha;
	double delta = 1;

	x = rand()% GSIZE;
	y = rand()% GSIZE;
	r = 1 +rand()%30 ;
	if(rand()%2==0) delta = -delta;

	for(i=y-r;i<=y+r;i++)
		for(j = x-r;j<=x+r;j++)
		{
			if(i>=0 && j>=0 && i<GSIZE && j<GSIZE)
			{
				dist = sqrt(((double)x-j)*(x-j) + (i-y)*(i-y));
				if(dist<r)
				{
					alpha = acos(dist/r);
					grid[i][j] += delta * 0.03* r*sin(alpha);
				}
			}
		}


}
void SetColor(double h)
{
	if(h>-5)
	{
		h=fabs(h);
		if(h > 0 && h < 0.4) // sand
			glColor3d(sand.RED,sand.GREEN,sand.BLUE);
		else if(h < 5)
			glColor3d(0.2+h/30,(5-h)/6,0);
		else 
			glColor3d(h/11,h/11,h/10);
	}
	else glColor3d(BLACK.RED,BLACK.GREEN,BLACK.BLUE);
}
void SetColor2d(double h)
{
	if(h > 0)
	{
		h=fabs(h);
		if(h > 0 && h < 0.4) // sand
			glColor3d(sand.RED,sand.GREEN,sand.BLUE);
		else if(h < 5)
			glColor3d(0.2+h/30,(5-h)/6,0);
		else 
			glColor3d(h/11,h/11,h/10);
	}
	else
	{
		glColor3d(BLUE.RED,BLUE.GREEN,BLUE.BLUE);
	}
}
// draws the buttons
void DrawMenu()
{
	int size = Menu_size;
	int x = Menu1_x;
	int y1 = Menu1_y;
	int y2 = Menu2_y;
	int y3 = Menu3_y;

	// Menu 1
	// Land down
	glColor3d(BLACK.RED,BLACK.GREEN,BLACK.BLUE);
	glBegin(GL_LINES);
		glVertex3d( x + 2,1,y1 + size/2);	
		glVertex3d( x + size - 2,1,y1 + size/2);
	glEnd();

	if(Menu_Selected == 1){
		glColor3d(buttonClicked.RED,buttonClicked.GREEN,buttonClicked.BLUE);
	}
	else {
		glColor3d(button.RED,button.GREEN,button.BLUE);
	}
	glBegin(GL_POLYGON);
		glVertex3d(x,1,y1);
		glVertex3d(x,1,y1+size);
		glVertex3d(x+size,1,y1+size);
		glVertex3d(x+size,1,y1);
	glEnd();
	// Menu 2
	// Land Up
	glColor3d(BLACK.RED,BLACK.GREEN,BLACK.BLUE);
	glBegin(GL_LINES);
		glVertex3d( x + 2,1,y2 + size/2);	
		glVertex3d( x + size - 2,1,y2 + size/2);
	glEnd();
	glBegin(GL_LINES);
		glVertex3d( x + size/2 ,1,y2 + 2);	
		glVertex3d( x + size/2,1,y2 + size - 2);
	glEnd();

	if(Menu_Selected == 2){
		glColor3d(buttonClicked.RED,buttonClicked.GREEN,buttonClicked.BLUE);
	}
	else {
		glColor3d(button.RED,button.GREEN,button.BLUE);
	}
	glBegin(GL_POLYGON);
		glVertex3d(x,1,y2);
		glVertex3d(x,1,y2+size);
		glVertex3d(x+size,1,y2+size);
		glVertex3d(x+size,1,y2);
	glEnd();
	// Menu 3
	// City
	glColor3d(BLACK.RED,BLACK.GREEN,BLACK.BLUE);
	glBegin(GL_LINES);
		glVertex3d( x + 2,1,y3 + size/3 + 1);	
		glVertex3d( x + 2,1,y3 + size - 1);
		glVertex3d( x + size - 2,1,y3 + size - 1);
		glVertex3d( x + size - 2,1,y3 + size/3+ 1);
		glVertex3d( x + 2,1, y3 + size/3 + 1);	
		glVertex3d( x + size - 2,1,y3 + size/3 + 1);
		glVertex3d( x + 2,1, y3 + size - 1);	
		glVertex3d( x + size - 2,1,y3 +size - 1);
		glVertex3d( x + 2,1, y3 + size/3 +1 );	
		glVertex3d( x + size/2 ,1,y3 + 1);
		glVertex3d( x + size/2 ,1,y3 + 1);
		glVertex3d( x + size - 2,1, y3 + size/3 + 1);	
		
	glEnd();

	if(Menu_Selected == 3){
		glColor3d(buttonClicked.RED,buttonClicked.GREEN,buttonClicked.BLUE);
	}
	else {
		glColor3d(button.RED,button.GREEN,button.BLUE);
	}
	glBegin(GL_POLYGON);
		glVertex3d(x,1,y3);
		glVertex3d(x,1,y3+size);
		glVertex3d(x+size,1,y3+size);
		glVertex3d(x+size,1,y3);
	glEnd();
}
// draws the ground for 3d mode
void DrawLand()
{
	int i,j;

	for(i=1;i<GSIZE;i++)
		for(j=1;j<GSIZE;j++)
		{
//			glBegin(GL_LINE_LOOP);
			glBegin(GL_POLYGON);
					SetColor(grid[i][j]);
				glVertex3d(j-GSIZE/2,grid[i][j],i-GSIZE/2);
					SetColor(grid[i][j-1]);
				glVertex3d(j-1-GSIZE/2,grid[i][j-1],i-GSIZE/2);
					SetColor(grid[i-1][j-1]);
				glVertex3d(j-1-GSIZE/2,grid[i-1][j-1],i-1-GSIZE/2);
					SetColor(grid[i-1][j]);
				glVertex3d(j-GSIZE/2,grid[i-1][j],i-1-GSIZE/2);
			glEnd();
		}

// water
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4d(0,0,0.5,0.7);
	glBegin(GL_POLYGON);
			glVertex3d(-GSIZE/2,0,-GSIZE/2);
			glVertex3d(GSIZE/2,0,-GSIZE/2);
			glVertex3d(GSIZE/2,0,GSIZE/2);
			glVertex3d(-GSIZE/2,0,GSIZE/2);
	glEnd();
	glDisable(GL_BLEND);
}
// draws the ground for 2d mode
void DrawLand2d()
{
	int i,j;

	for(i=1;i<GSIZE;i++)
		for(j=1;j<GSIZE;j++)
		{
			glBegin(GL_POLYGON);
					SetColor2d(grid[i][j]);
				glVertex3d(j-GSIZE/2,0,i-GSIZE/2);
					SetColor2d(grid[i][j-1]);
				glVertex3d(j-1-GSIZE/2,0,i-GSIZE/2);
					SetColor2d(grid[i-1][j-1]);
				glVertex3d(j-1-GSIZE/2,0,i-1-GSIZE/2);
					SetColor2d(grid[i-1][j]);
				glVertex3d(j-GSIZE/2,0,i-1-GSIZE/2);
			glEnd();
		}
}
// draw a wheel 
void DrawWheel(float width)
{
	double alpha,x,y;
	glColor3d(BLACK.RED,BLACK.GREEN,BLACK.BLUE);
	glBegin(GL_LINE_LOOP);
	for(alpha = 0; alpha<2*PI;alpha+=PI/20)
	{
		x = 0.7*cos(alpha);
		y = 0.7*sin(alpha);
		glVertex3d(0,x,y);
		glVertex3d(-width,x,y);
		
	}
	glEnd();

	glColor3d(BLACK.RED,BLACK.GREEN,BLACK.BLUE);
	glBegin(GL_LINES);
	for(alpha = 0; alpha<2*PI;alpha+=PI/25)
	{
		x = 0.7*cos(alpha);
		y = 0.7*sin(alpha);
		glVertex3d(0,x,y);
		glVertex3d(0,0,0);
	}
	glEnd();
}
// draw the car object
void DrawCar()
{
	// Body
	glEnable(GL_COLOR_MATERIAL);
	glMaterialf(GL_FRONT,GL_AMBIENT,0);
	glMaterialf(GL_FRONT,GL_DIFFUSE,0.1);
	glMaterialf(GL_FRONT,GL_SPECULAR,0.1);
	glMaterialf(GL_FRONT, GL_SHININESS, 1);
	glColor3d(WHITE.RED,WHITE.GREEN,WHITE.BLUE);
	glPushMatrix();

	// Front
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,9);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);			
		glTexCoord2d(0,1); glVertex3d(-2,2,0);			
		glTexCoord2d(1,1); glVertex3d(2,2,0); 
		glTexCoord2d(1,0); glVertex3d(2,0,0);		
		glTexCoord2d(0,0); glVertex3d(-2,0,0);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	// Back
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,5);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);			
		glTexCoord2d(1,1); glVertex3d(-2,2,6);			
		glTexCoord2d(0,1); glVertex3d(2,2,6); 
		glTexCoord2d(0,0); glVertex3d(2,0,6);		
		glTexCoord2d(1,0); glVertex3d(-2,0,6);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	// Left side
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,6);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);			
		glTexCoord2d(1,1); glVertex3d(-2,2,0);
		glTexCoord2d(0,1); glVertex3d(-2,2,6);
		glTexCoord2d(0,0); glVertex3d(-2,0,6); 
		glTexCoord2d(1,0); glVertex3d(-2,0,0);		
	glEnd();
	glDisable(GL_TEXTURE_2D);

	// right side
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,6);										
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);			
		glTexCoord2d(1,1); glVertex3d(2,2,0);
		glTexCoord2d(0,1); glVertex3d(2,2,6);
		glTexCoord2d(0,0); glVertex3d(2,0,6); 
		glTexCoord2d(1,0); glVertex3d(2,0,0);		
	glEnd();
	glDisable(GL_TEXTURE_2D);

	// Bottom side
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,4);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);			
		glTexCoord2d(0,0); glVertex3d(-2,0,0);
		glTexCoord2d(1,0); glVertex3d(-2,0,6);
		glTexCoord2d(1,1); glVertex3d(2,0,6); 
		glTexCoord2d(0,1); glVertex3d(2,0,0);		
	glEnd();
	glDisable(GL_TEXTURE_2D);

	// Up side
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,4);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);			
		glTexCoord2d(0,0); glVertex3d(-2,2,0);
		glTexCoord2d(1,0); glVertex3d(-2,2,6);
		glTexCoord2d(1,1); glVertex3d(2,2,6); 
		glTexCoord2d(0,1); glVertex3d(2,2,0);		
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();

	// =========================
	// Upper side
	glPushMatrix();
	glColor3d(WHITE.RED,WHITE.GREEN,WHITE.BLUE);

	// Front
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,3);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);			
		glTexCoord2d(0,1); glVertex3d(-1,4,2);			
		glTexCoord2d(1,1); glVertex3d(1,4,2); 
		glTexCoord2d(1,0); glVertex3d(2,2,1.5);		
		glTexCoord2d(0,0); glVertex3d(-2,2,1.5);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	// Back
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,2);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);			
		glTexCoord2d(1,1); glVertex3d(-1,4,5);			
		glTexCoord2d(0,1); glVertex3d(1,4,5); 
		glTexCoord2d(0,0); glVertex3d(2,2,5.5);		
		glTexCoord2d(1,0); glVertex3d(-2,2,5.5);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	// Left side
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,1);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);
		glTexCoord2d(0+0.4,1); glVertex3d(-1,4,2);
		glTexCoord2d(1,1); glVertex3d(-1,4,5);
		glTexCoord2d(1,0); glVertex3d(-2,2,5.5); 
		glTexCoord2d(0,0); glVertex3d(-2,2,1.5);				
	glEnd();
	glDisable(GL_TEXTURE_2D);

	// right side
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,11);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);		
		glTexCoord2d(1,1); glVertex3d(1,4,2);
		glTexCoord2d(0+0.4,1); glVertex3d(1,4,5);
		glTexCoord2d(0,0); glVertex3d(2,2,5.5); 
		glTexCoord2d(1,0); glVertex3d(2,2,1.5);		
	glEnd();
	glDisable(GL_TEXTURE_2D);

	// Up side
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,4);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glBegin(GL_POLYGON);			
		glTexCoord2d(0,0); glVertex3d(-1,4,2);
		glTexCoord2d(1,0); glVertex3d(-1,4,5);
		glTexCoord2d(1,1); glVertex3d(1,4,5); 
		glTexCoord2d(0,1); glVertex3d(1,4,2);		
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();

	
	// Wheels
	glPushMatrix();
	glTranslated(2.1,0,1);
	glRotated(angle*100,1,0,0);
	DrawWheel(0.5);
	glPopMatrix();

	glPushMatrix();
	glTranslated(2.1,0,5);
	glRotated(angle*100,1,0,0);
	DrawWheel(0.5);
	glPopMatrix();

	glPushMatrix();
	glTranslated(-2.1,0,1);
	glRotated(angle*100,1,0,0);
	DrawWheel(-0.5);
	glPopMatrix();

	glPushMatrix();
	glTranslated(-2.1,0,5);
	glRotated(angle*100,1,0,0);
	DrawWheel(-0.5);
	glPopMatrix();
}
void idle()
{
	int i,j;
	double r;
	angle+=dangle;
	start+=0.4;
	sightx = sin(angle);
	sightz = cos(angle);
	eyex+=sightx*speed;
	eyey+=dy;
	eyez+=sightz*speed;

	myCar_speed += 0.1;	// Speed

	glutPostRedisplay();
}
// decrese land
void LandDown(int x, int y)
{
	int i,j,r=7;
	double dist, ca, alpha, h;
 
	for(i=y-r+1; i<y+r; i++)
	{
		for(j=x-r+1; j<x+r; j++)
		{
			if(i >= 0 && i < GSIZE && j >= 0 && j < GSIZE)
			{
				dist = sqrt(double(x-j) * (x-j) + (y-i)*(y-i) );
				if(dist < r)
				{
					ca = dist/r;
					alpha = acos(ca);
					h = r * sin(alpha);

					h = (h/3);
					if(h > -5)
					{
						grid[i][j] -= h;
					}
				}
			}
		}
	}
}
// increase land 
void LandUp(int x, int y)
{
	int i,j,r=7;
	double dist, ca, alpha, h;
 
	for(i=y-r+1; i<y+r; i++)
	{
		for(j=x-r+1; j<x+r; j++)
		{
			if(i >= 0 && i < GSIZE && j >= 0 && j < GSIZE)
			{
				dist = sqrt(double(x-j) * (x-j) + (y-i)*(y-i) );
				if(dist < r)
				{
					ca = dist/r;
					alpha = acos(ca);
					h = r * sin(alpha);

					h = (h/3);
					if(grid[i][j] + h < 10)
					{
						grid[i][j] += h;
					}
				}
			}
		}
	}
}
// create houses in girdElements array
void SetHouses(int x, int y)
{
	if(grid[y][x] > 0)
	{
		// Check next to me
		for(int i1 = y-24; i1 <= y+24; i1++){
			for(int j1 = x-24; j1 <= x+24; j1++)
			{
				if(i1 >= 0 && j1 >= 0 && i1 < GSIZE && j1 < GSIZE)
					if(gridElements[i1][j1] > 0)
						return;
			}
		}
		gridElements[y][x] = 1;
		
		for(int i = 4; i<=8; i+=4)
		{
			int style = (rand() % 3) +2;
			if(y+i > 0 && y + i < GSIZE && x+i > 0 && x+i < GSIZE && grid[y+i][x+i] > 0)
				gridElements[y+i][x+i] = style;
			if(y-i > 0 && y - i < GSIZE && x-i > 0 && x-i < GSIZE && grid[y-i][x-i] > 0)
				gridElements[y-i][x-i] = style;
			if(y-i > 0 && y - i < GSIZE && x+i > 0 && x+i < GSIZE && grid[y-i][x+i] > 0)
				gridElements[y-i][x+i] = style;
			if(y+i > 0 && y + i < GSIZE && x-i > 0 && x-i < GSIZE && grid[y+i][x-i] > 0)
				gridElements[y+i][x-i] = style;
		}

		if(y+8 > 0 && y + 8 < GSIZE && x > 0 && x < GSIZE && grid[y+8][x] > 0)
			gridElements[y+8][x] =(rand() % 3) +2;
		if(y-8 > 0 && y - 8 < GSIZE && x > 0 && x < GSIZE && grid[y-8][x] > 0)
			gridElements[y-8][x] = (rand() % 3) +2;
		if(y > 0 && y < GSIZE && x+8 > 0 && x+8 < GSIZE && grid[y][x+8] > 0)
			gridElements[y][x+8] = (rand() % 3) +2;
		if(y > 0 && y < GSIZE && x-8 > 0 && x-8 < GSIZE && grid[y][x-8] > 0)
			gridElements[y][x-8] = (rand() % 3) +2;
		}
}
// this will draw a block according to n 
void Draw3dCylBlock(int n, double topR, double bottomR,int tXrepeat, double YStart, double YEnd, int txnum)
{
	double alpha,teta,xt,deltax = tXrepeat/ (double)n;
	teta = 2*PI/n;
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,txnum);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	for(alpha=0,xt =0;alpha<=2*PI;alpha+=teta,xt+=deltax)
	{
		glBegin(GL_POLYGON);
		glNormal3d(cos(alpha),0,sin(alpha));
		glTexCoord2d(xt,YStart);  glVertex3d(topR*cos(alpha),1,topR*sin(alpha));
		glTexCoord2d(xt+deltax,YStart);  glVertex3d(topR*cos(alpha+teta),1,topR*sin(alpha+teta));
		glTexCoord2d(xt+deltax,YEnd);  glVertex3d(bottomR*cos(alpha+teta),0,bottomR*sin(alpha+teta));
		glTexCoord2d(xt,YEnd); 	glVertex3d(bottomR*cos(alpha),0,bottomR*sin(alpha));
		glEnd();
	}
	glDisable(GL_TEXTURE_2D);

}
// draws a single house in 3d
void DrawHouse3d(int style)
{
	int walltexture;
	int rooftexture;
	int numOfWalls;
	switch (style){
	case 1: // road or main building
		walltexture = 7;
		rooftexture = 8;
		numOfWalls =4;
		break;
	case 2: //style 1
		walltexture = 13;
		rooftexture = 14;
		numOfWalls = 4;
		break;
	case 3: // style 2
		walltexture = 15;
		rooftexture = 16;
		numOfWalls = 60;
		break;
	case 4: // style 3
		walltexture = 7;
		rooftexture = 8;
		numOfWalls = 60;
		break;
	}
	glPushMatrix();
	glTranslated(0,-4,0);
	glScaled(1,8,1);
	Draw3dCylBlock(numOfWalls,RADIUS,RADIUS,18,0,8,walltexture);
	glPopMatrix();
	glPushMatrix();
	glTranslated(0,4,0);
	glScaled(1,4,1);
	Draw3dCylBlock(numOfWalls,0,RADIUS+0.1,1,0,1,rooftexture);
	glPopMatrix();
}
// wrapper function to draw all houses on grid
void DrawHouses()
{
	for(int i = 0; i < GSIZE; i++)
	for(int j = 0; j < GSIZE; j++)
	{
		double xCenter = i - (GSIZE/2);
		double yCenter = j - (GSIZE/2);

		if(gridElements[j][i] >= 1)
		{
			glPushMatrix();
			glEnable(GL_COLOR_MATERIAL);
			glMaterialf(GL_FRONT,GL_AMBIENT,1);
			glMaterialf(GL_FRONT,GL_DIFFUSE,1);
			glMaterialf(GL_FRONT,GL_SPECULAR,1);
			glMaterialf(GL_FRONT, GL_SHININESS, 1);
			glColor3d(WHITE.RED,WHITE.GREEN,WHITE.BLUE);
			glTranslated(xCenter,grid[j][i] + 4,yCenter);
			DrawHouse3d(gridElements[j][i]);
			glPopMatrix();
		}
	}
}
// draw the houses in 2d mode
void DrawElements()
{
	int r=2;
	GLint circle_points =100;
	glColor3d(BLACK.RED,BLACK.GREEN,BLACK.BLUE);
	double angle = 2*  PI/circle_points ;

	for(int i = 0; i < GSIZE; i++)
		for(int j = 0; j < GSIZE; j++)
		{
			if(gridElements[j][i] == 1 || gridElements[j][i] >= 2)
			{
				int xCenter = i - (GSIZE/2);
				int yCenter = j - (GSIZE/2);
				glBegin(GL_POLYGON);
					double angle1=0.0;
					glVertex3d(xCenter+(r* cos(0.0)) ,1,yCenter+(r* sin(0.0)));
					int i1;
					for ( i1=0 ; i1< circle_points ;i1++)
					{
						glVertex3d(xCenter+(r*cos(angle1)),1,yCenter+(r*sin(angle1)));
						angle1 += angle ;
					}
				glEnd();
			}
		}

}
void mouse(int button,int state,int x, int y)
{
	int xTemp, yTemp;
	x = x / (WIDTH/GSIZE);
	y = y / (HEIGHT/GSIZE);

	xTemp = x - (GSIZE/2);
	yTemp = y - (GSIZE/2);
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && xTemp >= Menu1_x && xTemp <= Menu1_x + Menu_size && yTemp >= Menu1_y && yTemp <= Menu1_y + Menu_size)
	{
		Menu_Selected = 1;
		return;
	}
	else if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && xTemp >= Menu2_x && xTemp <= Menu2_x + Menu_size && yTemp >= Menu2_y && yTemp <= Menu2_y + Menu_size)
	{
		Menu_Selected = 2;
		return;
	}
	else if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && xTemp >= Menu3_x && xTemp <= Menu3_x + Menu_size && yTemp >= Menu3_y && yTemp <= Menu3_y + Menu_size)
	{
		Menu_Selected = 3;
		return;
	}
	else if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		if(Menu_Selected == 1)
		{
			LandDown(x, y);
		}
		else if(Menu_Selected == 2)
		{
			LandUp(x, y);
		}
		else if(Menu_Selected == 3)
		{
			SetHouses(x, y);
		}
	}
}
void SpecialKey(int key,int x,int y)
{
	switch(key)
	{
	case GLUT_KEY_LEFT:
		dangle+=0.001;
		break;
	case GLUT_KEY_RIGHT:
		dangle-=0.001;
		break;
	case GLUT_KEY_UP:
		speed+=0.01;
		break;
	case GLUT_KEY_DOWN:
		speed-=0.01;
		break;
	case GLUT_KEY_PAGE_UP:
		dy+=0.01;
		break;
	case GLUT_KEY_PAGE_DOWN:
		dy-=0.01;
		break;
		
	}
}
void Keyboard(unsigned char key, int x, int y)
{
	switch( key)
	{
	case 'a':
			myCar_ang_speed+=0.002;
		break;
	case 'd':
			myCar_ang_speed-=0.002;
		break;
	case 'w':
			myCar_speed+=0.01;
		break;
	case 's':
			myCar_speed-=0.01;
		break;
	case 'r':
		eyex=1;eyey=10;eyez=20;
		angle=PI;
		resetCamera();
		break;
	case '1':
		viewFrom = 0;	// Camera view
		break;
	case '2':
		viewFrom = 1;
		break;
	case '3':
		viewFrom = 2;
		break;
	case '4':
		viewFrom = 3;
		break;
	case '5':
		viewFrom = 4;
		break;
	}
}
void Add2RoadDrive(double x, double y,double x1,double y1,double h1,double a1)
{	//  										0    1 2     3        4      5 6 7 8
	// int roadDriveArray[GSIZE][9]={0}; //  Active  X|Y  | Index  | Count  |X|Y|Z|A|
	int i;
	int iCount = 1;
	int iIndex = 1;
	for(i = 0; i < GSIZE - 1; i++)
	{
		if(roadDriveArray[i][0] == 1)
		{
			// Update
			if(roadDriveArray[i][1] == x && roadDriveArray[i][2] == y )
			{
				roadDriveArray[i][4]++;	// Count
				iCount = roadDriveArray[i][4];
				iIndex++;
			}
		}
		else
		{
			break;
		}
	}

	// Add
	roadDriveArray[i][0] = 1;
	roadDriveArray[i][1] = x;
	roadDriveArray[i][2] = y;
	roadDriveArray[i][3] = iIndex;
	roadDriveArray[i][4] = iCount;
	roadDriveArray[i][5] = x1;
	roadDriveArray[i][6] = y1;
	roadDriveArray[i][7] = h1;
	roadDriveArray[i][8] = a1;
}
void MoveCar(double x, double y, bool IsLastPoint, double x1, double y1, double h1, double a1)
{
	if(myCarx == -1 && myCar_angle == -1)
		return;

	if(IsLastPoint == false)
	{
		if(myCar_speed > 1 && myCarx==y && myCarz == x)
		{
			myCar_speed = 0;
			myCarx = y1;
			if(h1 < 0)
				h1 = 1;
			myCary = h1 + 0.5;
			myCarz = x1;
			myCar_angle = a1;
		}
	}
	else
	{
		int iIndex = -1;
		for(int i=0; i < GSIZE; i++)
		{
			if(roadDriveArray[i][0] == 0)
				break;
			if(roadDriveArray[i][0] == 1 && roadDriveArray[i][1] == x && roadDriveArray[i][2] == y)
			{
				if(iIndex == -1)
				{
					int iCount =  roadDriveArray[i][4];
					iIndex = rand() % iCount + 1;
				}
				if(iIndex == roadDriveArray[i][3])
				{
					myCar_speed = 0;
					myCarx = roadDriveArray[i][6];
					h1 = roadDriveArray[i][7];
					if(h1 < 0)
						h1 = 1;
					myCary = h1 + 0.5;
					myCarz = roadDriveArray[i][5];
					myCar_angle = roadDriveArray[i][8];
					break;
				}
			}
		}
	}
}
void DrawRoad(int x1, int y1, int x2, int y2)
{
	x1 = ((GSIZE/2) - x1) * -1;
	x2 = ((GSIZE/2) - x2) * -1;
	y2 = (GSIZE/2) - y2;
	y1 = (GSIZE/2) - y1;
	double a,b;
	double x,y;
	double start, stop;
	int iCount = 0;
	double Roadx[GSIZE*2] = {-1};
	double Roady[GSIZE*2] = {-1};

	if(x2 - x1 == 0)
	{
		a = 0;
		return;
	}
	else
	{
		a = double(y2 - y1) / double(x2 - x1);
	}
	b = y1 - a * x1;

	if(fabs(a)<=1)
	{
		if(x1 > x2)
		{
			start = x2;
			stop = x1;
		}
		else
		{
			start = x1;
			stop = x2;
		 }

		for(x = start; x <= stop; x++)
		{
			y = a * x + b;
			Roadx[iCount] = x;
			Roady[iCount] = y * -1;

			iCount++;
		}
	}
	else
	{
		if(y1 > y2)
		{
			start = y2;
			stop = y1;
		}
		else
		 {
			start = y1;
			stop = y2;
		}

		for(y = start; y <= stop; y++)
		{
			x = (y - b) / a;

			Roadx[iCount] = x;
			Roady[iCount] = y * -1;

			iCount++;
		}
	}

	glColor3d(WHITE.RED,WHITE.GREEN,WHITE.BLUE);
	double h = 0.04;

	double car_angle_temp = -(atan (a) * 180 / PI);
	if(a > 0)
		car_angle_temp += 180;

	if(Is1st3D == false)
	{
		myCarx = -1;
		myCar_angle = -1;
		int yGrid_temp = (GSIZE/2) + Roady[1];
		int xGrid_temp = (GSIZE/2) + Roadx[1];
		Add2RoadDrive(Roadx[0],Roady[0],Roadx[1],Roady[1],grid[yGrid_temp][xGrid_temp],car_angle_temp);

		car_angle_temp -= 180;
		if(car_angle_temp < 0)
			car_angle_temp += 360;
		int yGrid1_temp = (GSIZE/2) + Roady[iCount - 2];
		int xGrid1_temp = (GSIZE/2) + Roadx[iCount - 2];
		Add2RoadDrive(Roadx[iCount - 1],Roady[iCount - 1],Roadx[iCount - 2],Roady[iCount - 2],grid[yGrid1_temp][xGrid1_temp],car_angle_temp);
	}

	for(int i =0; i < iCount - 1; i++)
	{
		int yGrid, xGrid;
		yGrid = (GSIZE/2) + Roady[i];
		xGrid = (GSIZE/2) + Roadx[i];

		// Check if the car is on the last point
		bool IsLastPoint = false;
		if(i == 0)
		{
			if(myCarx == Roady[0] && myCarz == Roadx[0])
				IsLastPoint = true;
		}
		if(i == iCount - 2)
		{
			if(myCarx == Roady[i+1] && myCarz == Roadx[i+1])
				IsLastPoint = true;
		}

		if(car_angle_temp == myCar_angle)
		{
			if(IsLastPoint == true)
				MoveCar(Roadx[i+1], Roady[i+1], IsLastPoint, Roadx[i+1], Roady[i+1],grid[xGrid][yGrid],myCar_angle);
			else
				MoveCar(Roadx[i], Roady[i], IsLastPoint, Roadx[i+1], Roady[i+1],grid[xGrid][yGrid],myCar_angle);
		}
		else
		{
			int yGrid_temp = (GSIZE/2) + Roady[i-1];
			int xGrid_temp = (GSIZE/2) + Roadx[i-1];
			MoveCar(Roadx[i], Roady[i], IsLastPoint, Roadx[i-1], Roady[i-1],grid[xGrid_temp][yGrid_temp],myCar_angle);
		}

		glPushMatrix();
		glBegin(GL_LINES);
		if(grid[xGrid][yGrid]<0)  // bridge
		{
			glVertex3d(Roady[i],1,Roadx[i]);
			glVertex3d(Roady[i+1],1,Roadx[i+1]);
		}
		else
		{
			glVertex3d(Roady[i],grid[xGrid][yGrid] + h,Roadx[i]);
			glVertex3d(Roady[i+1],grid[xGrid][yGrid] + h,Roadx[i+1]);
		}

		glEnd();
		glPopMatrix();

		if(Is1st3D == false)
		{
			// Get car start point on the road
			if(myCarx == -1 && myCar_angle == -1 && i > 5)
			{
				myCarx = Roady[i];
				myCary = 1 + 0.5;
				myCarz = Roadx[i];
				myCar_angle = -(atan (a) * 180 / PI);
				if(a > 0)
					myCar_angle += 180;
			}
		}

		// Fix floor under the road (only once)
		if(Is1st3D == false)
		{
			int i9,j9,r=4;
			double dist9;
			double y = Roadx[i];
			double x = Roady[i];

			x = x + (GSIZE/2);
			y = y + (GSIZE/2);

			for(i9=y-r+1; i9<y+r; i9++)
			{
				for(j9=x-r+1; j9<x+r; j9++)
				{
					if(i9 >= 0 && i9 < GSIZE && j9 >= 0 && j9 < GSIZE)
					{
						dist9 = sqrt(double(x-j9) * (x-j9) + (y-i9)*(y-i9) );
						if(dist9 < r)
						{
							if(grid[i9][j9] > 0)
							{
								grid[i9][j9] = 1;
							}
						}
					}
				}
			}
		}


		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		if(grid[xGrid][yGrid]<0)
			glBindTexture(GL_TEXTURE_2D,12);
		else
			glBindTexture(GL_TEXTURE_2D,0);
		glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
		glMaterialf(GL_FRONT,GL_AMBIENT,0);
		glMaterialf(GL_FRONT,GL_DIFFUSE,0.1);
		glMaterialf(GL_FRONT,GL_SPECULAR,0.1);
		glMaterialf(GL_FRONT, GL_SHININESS, 1);
		glColor3d(WHITE.RED,WHITE.GREEN,WHITE.BLUE);

		glTranslated(Roady[i],0.1,Roadx[i]);
		glRotated(car_angle_temp,0,1,0);
		double dist = GetDistanceBetweenPoints(Roadx[i],Roady[i],Roadx[i+1],Roady[i+1]);
		glScaled(1,1,dist);
		glBegin(GL_POLYGON);
			glTexCoord2d(0,1); glVertex3d(-3,1,0.5);
			glTexCoord2d(0,0); glVertex3d(3,1,0.5);
			glTexCoord2d(1,0); glVertex3d(3,1,-0.5);
			glTexCoord2d(1,1); glVertex3d(-3,1,-0.5);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		glPopMatrix();
	}
}
void DrawRoads()
{
	int houseArry[GSIZE][2] = {-1};
	int iCount = 0;

	int x1,y1,x2,y2;
	x1 = -1;
	for(int i = 0; i < GSIZE; i++)
	for(int j = 0; j < GSIZE; j++)
	{
		if(gridElements[j][i] == 1)
		{
			houseArry[iCount][0] = j;
			houseArry[iCount][1] = i;
			iCount++;
		}
	}

	const int iPointsCount = 4;
	int points1[GSIZE][iPointsCount];

	for(int i=0; i < GSIZE; i++)
	for(int k=0; k < iPointsCount; k++)
		points1[i][k] = -1;

	for(int i=0; i < iCount; i++)
	{
		int x1 = houseArry[i][0];
		int y1 = houseArry[i][1];
		for(int m=0; m < 2; m++)
		{
			double dMin = -1;
			int jMin = -1;
			for(int j=0; j < iCount; j++)
			{
				if(i != j)
				{
					int bCheck = -1;
					for(int k=0; k < iPointsCount; k++)
					{
						if(points1[i][k] == j)
						{
							bCheck = 1;
							break;
						}
						if(points1[j][k] == i)
						{
							bCheck = 1;
							break;
						}
					}
					if(bCheck == 1)
						continue;

					int x2 = houseArry[j][0];
					int y2 = houseArry[j][1];
					double d = GetDistanceBetweenPoints(x1,y1,x2,y2);
					if(d < dMin || dMin == -1)
					{
						dMin = d;
						jMin = j;
					}
				}
			}
			if(dMin != -1)
			{
				int x2 = houseArry[jMin][0];
				int y2 = houseArry[jMin][1];
				DrawRoad(x1, y1, x2, y2);

				for(int k=0; k < iPointsCount; k++)
				{
					if(points1[i][k] == -1)
					{
						points1[i][k] = jMin;
						break;
					}
				}
			}
		}
	}

}
void display2D()
{
	Is1st3D = false;
	// fill the buffer with the background color
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1,1,-1,1,1,200);
	gluLookAt(0,100,0,0,0,0,0,0,-1);
	//gluLookAt(0,0,100,0,0,0,-1,0,0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	DrawElements();
	DrawMenu();
	DrawLand2d();
	glutSwapBuffers();
}
void display3D()
{
	// fill the buffer with the background color
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1,1,-1,1,1,200);

	if(inCar == false)
	{
		switch(viewFrom) {
		case 0:
			gluLookAt(eyex,eyey,eyez,eyex+sightx,eyey+sighty,eyez+sightz,0,1,0);
			break;
		case 1:
			resetCamera();
			gluLookAt(0,40,50, 0,0,0, 0,1,0);
			break;
		case 2:
			resetCamera();
			gluLookAt(50,40,0, 0,0,0, 0,1,0);
			break;
	
		case 3:
			resetCamera();
			gluLookAt(0,40,-50, 0,0,0, 0,1,0);
			break;
		default: 
			resetCamera();
			gluLookAt(-50,40,0, 0,0,0, 0,1,0);
		}
		//gluLookAt(0.86,3.59,7.25,0.73,3.39,6.26,0,1,0);	// Center on the car
	}
	else
	{
		resetCamera();
		gluLookAt(
			myCarx-4*sin( (myCar_angle+180)/180*PI),
			myCary + 5,
			myCarz-4*cos( (myCar_angle+180)/180*PI),
		
			myCarx+15*sin( (myCar_angle+180)/180*PI),
			myCary+2.9,
			myCarz+15*cos( (myCar_angle+180)/180*PI),
		0,1,0);
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
		DrawLand();
	glPopMatrix();

	glPushMatrix();
		DrawRoads();
	glPopMatrix();

	glPushMatrix();
		DrawHouses();
	glPopMatrix();

	glPushMatrix();
		glTranslated(myCarx,myCary+0.1, myCarz);
		glRotated(myCar_angle,0,1,0);
		glScaled(0.7,0.7,0.7);
		DrawCar();
	glPopMatrix();

	Is1st3D = true;
	glutSwapBuffers();
}
void resetCamera()
{
	dangle =0;
	speed = 0;
	dy = 0;

}
void menu(int option)
{
	switch(option)
	{
	case 1:
		if(lastOption != 1)
			for(int i1 = 0; i1 < GSIZE; i1++)
				for(int j1 = 0; j1 < GSIZE; j1++)
					grid[i1][j1] = gridBK[i1][j1];
		glutDisplayFunc(display2D);
		lastOption = 1;
		break;
	case 2:
		sighty= -0.2;
		if(lastOption == 1)
		{
			for(int i1 = 0; i1 < GSIZE; i1++)
				for(int j1 = 0; j1 < GSIZE; j1++)
					gridBK[i1][j1] = grid[i1][j1];

			for(int i1 = 0; i1 < GSIZE; i1++)
				roadDriveArray[i1][0] = 0;
		}
		inCar = false;
		glutDisplayFunc(display3D);
		lastOption = 2;
		break;
	case 3:
		if(lastOption == 1)
		{
			for(int i1 = 0; i1 < GSIZE; i1++)
				for(int j1 = 0; j1 < GSIZE; j1++)
					gridBK[i1][j1] = grid[i1][j1];

			for(int i1 = 0; i1 < GSIZE; i1++)
				roadDriveArray[i1][0] = 0;
		}
		inCar = true;
		glutDisplayFunc(display3D);
		lastOption = 3;
		break;
	}
}


void main(int argc, char** argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(WIDTH,HEIGHT);
	glutInitWindowPosition(100,100);
	glutCreateWindow("City");

	// onPaint
	glutDisplayFunc(display2D);
	// onTimer
	glutIdleFunc(idle);
	glutMouseFunc(mouse);
	glutSpecialFunc(SpecialKey);
	glutKeyboardFunc(Keyboard);

	glutCreateMenu(menu);
	glutAddMenuEntry("2D",1);
	glutAddMenuEntry("3D",2);
	glutAddMenuEntry("Car",3);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	init();
	glutMainLoop();
}