const int TW = 128;
const int TH = 128;

const int PICW = 256;
const int PICH = 256;

const int CARRUW = 355;
const int CARRUH = 62;

const int CARBUW = 200;
const int CARBUH = 73;

const int CARFUW = 200;
const int CARFUH = 71;

const int CARW = 283;
const int CARH = 167;

const int CARFDW = 200;
const int CARFDH = 78;

const int CARBDW = 200;
const int CARBDH = 91;

const int CARLDW = 428;
const int CARLDH = 68;

const int SKYW = 512;
const int SKYH = 512;

unsigned char texture[TH][TW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char car_left1[CARRUH][CARRUW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char car_back1[CARBUH][CARBUW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char car_front2[CARFUH][CARFUW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char car_up[CARH][CARW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char car_back[CARBDH][CARBDW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char car_left[CARLDH][CARLDW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char wall2[PICH][PICW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char roof2[PICH][PICW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char car_front[CARFDH][CARFDW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char car_right[CARLDH][CARLDW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char car_right2[CARRUH][CARRUW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char road_bridge[TH][TW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char wall[PICH][PICW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char roof[PICH][PICW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char wall3[PICH][PICW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char roof3[PICH][PICW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char sky[5][SKYH][SKYW][4] = {0}; // texture based on R,G,B,Alpha
unsigned char* pic;

void LoadBitmap(char* fname)
{
	BITMAPFILEHEADER bf;
	BITMAPINFOHEADER bi;
	int sz;
	FILE* f;
	
	f= fopen(fname,"rb");

	fread(&bf,sizeof(bf),1,f);
	fread(&bi,sizeof(bi),1,f);
	sz = bi.biWidth*bi.biHeight *3; // num of pixels * (b,g,r)
	pic = (unsigned char*) malloc(sz);

	fread(pic,1,sz,f); // read raw data from file

	fclose(f);
}

void CreateTexture(int tnum,int subnum)
{
	int i,j,k;
	int w=10, h = TW/4;
	switch(tnum) // number of texture
	{
		case 0: // road 
			for(i=0;i<TH;i++)
				for(j=0;j<TW;j++)
				{
					if(i>=TH/2-w && i<=TH/2+w && (j<h || j>TW-h))
					{
						texture[i][j][0] = 255; // red
						texture[i][j][1] = 255; // green
						texture[i][j][2] = 255; // blue
						texture[i][j][3] = 0; // alpha
					}
					else
					{
						k = 80+(rand()%40) ;
						texture[i][j][0] = k; // red
						texture[i][j][1] = k; // green
						texture[i][j][2] = k; // blue
						texture[i][j][3] = 0; // alpha
					}
				}
			break;
		case 1:
			for(i=0,k=0;i<CARRUH;i++)
				for(j=0;j<CARRUW;j++)
				{
					car_left1[i][j][0] = pic[k+2];
					car_left1[i][j][1] = pic[k+1];
					car_left1[i][j][2] = pic[k];
					car_left1[i][j][3] = 0;
					k+=3;
				}
		break;
		case 2:
			for(i=0,k=0;i<CARBUH;i++)
				for(j=0;j<CARBUW;j++)
				{
					car_back1[i][j][0] = pic[k+2];
					car_back1[i][j][1] = pic[k+1];
					car_back1[i][j][2] = pic[k];
					car_back1[i][j][3] = 0;
					k+=3;
				}
		break;
		case 3:
			for(i=0,k=0;i<CARFUH;i++)
				for(j=0;j<CARFUW;j++)
				{
					car_front2[i][j][0] = pic[k+2];
					car_front2[i][j][1] = pic[k+1];
					car_front2[i][j][2] = pic[k];
					car_front2[i][j][3] = 0;
					k+=3;
				}
		break;
		case 4:
			for(i=0,k=0;i<CARH;i++)
				for(j=0;j<CARW;j++)
				{
					car_up[i][j][0] = pic[k+2];
					car_up[i][j][1] = pic[k+1];
					car_up[i][j][2] = pic[k];
					car_up[i][j][3] = 0;
					k+=3;
				}
		break;
		case 5:
			for(i=0,k=0;i<CARBDH;i++)
				for(j=0;j<CARBDW;j++)
				{
					car_back[i][j][0] = pic[k+2];
					car_back[i][j][1] = pic[k+1];
					car_back[i][j][2] = pic[k];
					car_back[i][j][3] = 0;
					k+=3;
				}
		break;
		case 6:
			for(i=0,k=0;i<CARLDH;i++)
				for(j=0;j<CARLDW;j++)
				{
					car_left[i][j][0] = pic[k+2];
					car_left[i][j][1] = pic[k+1];
					car_left[i][j][2] = pic[k];
					car_left[i][j][3] = 0;
					k+=3;
				}
		break;
		case 7:
			for(i=0,k=0;i<PICH;i++)
				for(j=0;j<PICW;j++)
				{
					wall2[i][j][0] = pic[k+2];
					wall2[i][j][1] = pic[k+1];
					wall2[i][j][2] = pic[k];
					wall2[i][j][3] = 0;
					k+=3;
				}
		break;
		case 8:
			for(i=0,k=0;i<PICH;i++)
				for(j=0;j<PICW;j++)
				{
					roof2[i][j][0] = pic[k+2];
					roof2[i][j][1] = pic[k+1];
					roof2[i][j][2] = pic[k];
					roof2[i][j][3] = 0;
					k+=3;
				}
		break;
		case 9:
			for(i=0,k=0;i<CARFDH;i++)
				for(j=0;j<CARFDW;j++)
				{
					car_front[i][j][0] = pic[k+2];
					car_front[i][j][1] = pic[k+1];
					car_front[i][j][2] = pic[k];
					car_front[i][j][3] = 0;
					k+=3;
				}
		break;
		case 10:
			for(i=0,k=0;i<CARLDH;i++)
				for(j=0;j<CARLDW;j++)
				{
					car_right[i][j][0] = pic[k+2];
					car_right[i][j][1] = pic[k+1];
					car_right[i][j][2] = pic[k];
					car_right[i][j][3] = 0;
					k+=3;
				}
		break;
		case 11:
			for(i=0,k=0;i<CARRUH;i++)
				for(j=0;j<CARRUW;j++)
				{
					car_right2[i][j][0] = pic[k+2];
					car_right2[i][j][1] = pic[k+1];
					car_right2[i][j][2] = pic[k];
					car_right2[i][j][3] = 0;
					k+=3;
				}
		break;
		case 12: // road bridge
			for(i=0;i<TH;i++)
				for(j=0;j<TW;j++)
				{
					if(i>=TH/2-w && i<=TH/2+w && (j<h || j>TW-h))
					{
						road_bridge[i][j][0] = 0; // red
						road_bridge[i][j][1] = 0; // green
						road_bridge[i][j][2] = 255; // blue
						road_bridge[i][j][3] = 0; // alpha
					}
					else
					{
						k = 80+(rand()%40) ;
						road_bridge[i][j][0] = k; // red
						road_bridge[i][j][1] = k; // green
						road_bridge[i][j][2] = k; // blue
						road_bridge[i][j][3] = 0; // alpha
					}

				}
			break;
		case 13:
			for(i=0,k=0;i<PICH;i++)
				for(j=0;j<PICW;j++)
				{
					wall[i][j][0] = pic[k+2];
					wall[i][j][1] = pic[k+1];
					wall[i][j][2] = pic[k];
					wall[i][j][3] = 0;
					k+=3;
				}
		break;
		case 14:
			for(i=0,k=0;i<PICH;i++)
				for(j=0;j<PICW;j++)
				{
					roof[i][j][0] = pic[k+2];
					roof[i][j][1] = pic[k+1];
					roof[i][j][2] = pic[k];
					roof[i][j][3] = 0;
					k+=3;
				}
		break;
		case 15:
			for(i=0,k=0;i<PICH;i++)
				for(j=0;j<PICW;j++)
				{
					wall3[i][j][0] = pic[k+2];
					wall3[i][j][1] = pic[k+1];
					wall3[i][j][2] = pic[k];
					wall3[i][j][3] = 0;
					k+=3;
				}
		break;
		case 16:
			for(i=0,k=0;i<PICH;i++)
				for(j=0;j<PICW;j++)
				{
					roof3[i][j][0] = pic[k+2];
					roof3[i][j][1] = pic[k+1];
					roof3[i][j][2] = pic[k];
					roof3[i][j][3] = 0;
					k+=3;
				}
		break;

		case 17:
				for(i=0,k=0;i<SKYH;i++)
					for(j=0;j<SKYW;j++)
					{
 						sky[subnum][i][j][0] = pic[k+2];
						sky[subnum][i][j][1] = pic[k+1];
						sky[subnum][i][j][2] = pic[k];
						sky[subnum][i][j][3] = 0;
						k+=3;
					}
		break;


	}
}

void TexParams()
{
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
}

void InitTextures()
{
	printf("initilizing textures...\n");
	CreateTexture(0,0);
	glBindTexture(GL_TEXTURE_2D,0);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,TW,TH,0,GL_RGBA,GL_UNSIGNED_BYTE,texture);
	LoadBitmap(".\\Textures\\left2.bmp");
	CreateTexture(1,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,1);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CARRUW,CARRUH,0,GL_RGBA,GL_UNSIGNED_BYTE,car_left1);

	LoadBitmap(".\\Textures\\back1.bmp");
	CreateTexture(2,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,2);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CARBUW,CARBUH,0,GL_RGBA,GL_UNSIGNED_BYTE,car_back1);

	LoadBitmap(".\\Textures\\front1.bmp");
	CreateTexture(3,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,3);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CARFUW,CARFUH,0,GL_RGBA,GL_UNSIGNED_BYTE,car_front2);

	LoadBitmap(".\\Textures\\UP.bmp");
	CreateTexture(4,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,4);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CARW,CARH,0,GL_RGBA,GL_UNSIGNED_BYTE,car_up);

	LoadBitmap(".\\Textures\\back.bmp");
	CreateTexture(5,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,5);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CARBDW,CARBDH,0,GL_RGBA,GL_UNSIGNED_BYTE,car_back);

	LoadBitmap(".\\Textures\\left.bmp");
	CreateTexture(6,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,6);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CARLDW,CARLDH,0,GL_RGBA,GL_UNSIGNED_BYTE,car_left);

	
	LoadBitmap(".\\Textures\\wall2.bmp");
	CreateTexture(7,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,7);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,wall2);

	LoadBitmap(".\\Textures\\roof2.bmp");
	CreateTexture(8,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,8);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,roof2);

	LoadBitmap(".\\Textures\\front.bmp");
	CreateTexture(9,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,9);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CARFDW,CARFDH,0,GL_RGBA,GL_UNSIGNED_BYTE,car_front);

	LoadBitmap(".\\Textures\\right.bmp");
	CreateTexture(10,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,10);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CARLDW,CARLDH,0,GL_RGBA,GL_UNSIGNED_BYTE,car_right);

	LoadBitmap(".\\Textures\\right2.bmp");
	CreateTexture(11,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,11);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CARRUW,CARRUH,0,GL_RGBA,GL_UNSIGNED_BYTE,car_right2);

	CreateTexture(12,0);
	glBindTexture(GL_TEXTURE_2D,12);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,TW,TH,0,GL_RGBA,GL_UNSIGNED_BYTE,road_bridge);
	
	LoadBitmap(".\\Textures\\wall.bmp");
	CreateTexture(13,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,13);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,wall);

	LoadBitmap(".\\Textures\\roof.bmp");
	CreateTexture(14,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,14);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,roof);
	
	LoadBitmap(".\\Textures\\wall3.bmp");
	CreateTexture(15,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,15);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,wall3);

	LoadBitmap(".\\Textures\\roof3.bmp");
	CreateTexture(16,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,16);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,roof3);
	//sky 
	LoadBitmap(".\\Textures\\sky\\backav9.bmp");
	CreateTexture(17,0);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,17);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,sky[0]);

	LoadBitmap(".\\Textures\\sky\\backav9.bmp");
	CreateTexture(17,1);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,18);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,sky[1]);
	
	LoadBitmap(".\\Textures\\sky\\backav9.bmp");
	CreateTexture(17,2);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,19);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,sky[2]);

	LoadBitmap(".\\Textures\\sky\\backav9.bmp");
	CreateTexture(17,3);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,20);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,sky[3]);

	LoadBitmap(".\\Textures\\sky\\backav9.bmp");
	CreateTexture(17,4);
	free(pic);
	glBindTexture(GL_TEXTURE_2D,21);
	TexParams();
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,PICW,PICH,0,GL_RGBA,GL_UNSIGNED_BYTE,sky[4]);
}