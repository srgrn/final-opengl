typedef struct {
	float RED;
	float GREEN;
	float BLUE;
	float alpha;
} color;


color BLACK = {0,0,0};
color WHITE = {1.0,1.0,1.0};
color BLUE = {0,0,1.0};
color GREEN = {0,1.0,0};
color RED = {1.0,0,0};

color sand = {0.8,0.8,0.5};
color button = {0.6,1.0,1.0};
color buttonClicked = {1.0,0.4,0};
color Background = {0.482,0.407,0.93333,0.1};

